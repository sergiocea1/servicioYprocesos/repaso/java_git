Vamos a realizar la siguiente tarea a modo repaso:

Un pequeño programa en Java con tres hitos

- Declaración e inicialización de un array de enteros de tamaño 10

Primer HITO: Introducción de valores en el array y visualización de los mismos: en cada posición el contenido será su número de posición
Segundo HITO: Modificación del array y visualización del mismo: en las posiciones pares del array se modificará el contenido que tenga por 0
Tercer HITO:
- Visualización del número de ceros que hay en el array.
- Visualización del número de elementos distintos de cero que hay en el array.

EN LOS COMMITS el mensaje será HITO + número de hito!!!
